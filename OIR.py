import os
import os.path
import subprocess
from itertools import izip
from PIL import Image
import base64
import cv2
import numpy as np

class OIR:

    def imgExistsIn(self, original, find):
        img_rgb = cv2.imread(original)
        #img_rgb = cv2.imread('/path/to/img/base.png')
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
        #template = cv2.imread('/path/to/img/variable.png', 0)
        template = cv2.imread(find, 0)
        w, h = template.shape[::-1]
        res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
        threshold = 0.8
        loc = np.where( res >= threshold)
        return True if len(loc[0]) > 0 else False

