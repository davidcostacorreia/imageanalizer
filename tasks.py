
import os
import datetime
from OIR import OIR
from celery import Celery
from config import compareImage
from config import rabbitSettings
from elasticsearch import Elasticsearch

app = Celery()
app.conf.broker_url = 'amqp://%s:%s@%s:%s/%s' % (rabbitSettings['user'], rabbitSettings['passwd'],
                                                    rabbitSettings['host'], rabbitSettings['port'],
                                                    rabbitSettings['vhost'])

@app.task
def processLog(data):
    elastic = Elasticsearch(['localhost'])

    now = datetime.date.today()
    index = '%s-%s.%s.%s' % (
    	'imageanalizer',
      	now.year, 
       	now.month if now.month > 9 else '0%s' % (now.month),
       	now.day if now.day > 9 else '0%s' % (now.day)
       )
    file = data['_data']['file']
    img = compareImage
    dataToElastic = {
        'file':           file,
        'result':         OIR().imgExistsIn(original=img, find=file),
        'sendToRabbitAt': data['_data']['sendToRabbitAt']
    }

    diff = datetime.datetime.now() - datetime.datetime.strptime(data['_data']['sendToRabbitAt'], '%Y-%m-%dT%H:%M:%S.%f')
    #cast float('seconds.microsec')
    diff = float(str(diff.seconds) + '.' + str(diff.microseconds)) 
    dataToElastic['calcTime'] = diff
    dataToElastic['finishAt'] = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')

    result = elastic.index(index=index, doc_type='log', body=dataToElastic)

    os.remove(file)
    return True





