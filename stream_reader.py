
#ffmpeg -i "udp://127.0.0.1:5000" -vf fps=25 /opt/imageanalizer/images/img%09d.jpg 
from ffmpy import FFmpeg
from config import streaming
ff = FFmpeg(
    inputs={streaming['streamer']['udp']: None},
    outputs={streaming['stream_reader']['imgPath']: streaming['stream_reader']['streamingOutputArgs']}
)
#print ff.cmd
ff.run()