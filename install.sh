
echo "********** Install ElasticSearch **********"

sudo yum install java-1.8.0-openjdk.x86_64 -y
sudo rpm -ivh https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.2.3.rpm 

sudo systemctl enable elasticsearch.service 
sudo systemctl start elasticsearch.service 


echo "********** Install RabbitMQ **********"

cd ~ 
wget http://packages.erlang-solutions.com/erlang-solutions-1.0-1.noarch.rpm 
sudo rpm -Uvh erlang-solutions-1.0-1.noarch.rpm 
sudo yum install erlang -y 
 

cd ~ 
wget https://www.rabbitmq.com/releases/rabbitmq-server/v3.6.1/rabbitmq-server-3.6.1-1.noarch.rpm 
sudo rpm --import https://www.rabbitmq.com/rabbitmq-signing-key-public.asc 
sudo yum install rabbitmq-server-3.6.1-1.noarch.rpm -y 

 

sudo firewall-cmd --zone=public --permanent --add-port=4369/tcp --add-port=25672/tcp --add-port=5671-5672/tcp --add-port=15672/tcp  --add-port=61613-61614/tcp --add-port=1883/tcp --add-port=8883/tcp 
sudo firewall-cmd --reload 
 

sudo systemctl start rabbitmq-server.service 
sudo systemctl enable rabbitmq-server.service 
 

sudo rabbitmq-plugins enable rabbitmq_management 
sudo chown -R rabbitmq:rabbitmq /var/lib/rabbitmq/ 
 

sudo rabbitmqctl add_user admin admin 
sudo rabbitmqctl set_user_tags admin administrator 
sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"

echo "********** Install Ffmpeg **********"

sudo yum install ffmpeg ffmpeg-devel –y 


#echo "********** Getting project's files from bitbucket **********"

#cd /opt/ && git clone https://davidcostacorreia@bitbucket.org/davidcostacorreia/imageanalizer.git
#cd imageanalizer
#pip install -r requirements.txt

#echo "********** Get a video and transform file **********"

cd /opt/imageanalizer && mkdir tmp && cd tmp
wget http://inspiringfuture.pt/uploads/university/logo/265/ISTEC.png && ffmpeg -loop 1 -i ISTEC.png -c:v libx264 -b 800k -t 5 -pix_fmt yuv420p -vf scale=370:-1 imageVideo.mp4 && ffmpeg -i http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4 -ss 00:00:30 -t 15 BigBuckBunny.mp4 && ffmpeg -i imageVideo.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate1.ts && ffmpeg -i BigBuckBunny.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate2.ts && ffmpeg -i "concat:intermediate1.ts|intermediate2.ts" -c copy -bsf:a aac_adtstoasc output.mp4 && rm ISTEC.png && ffmpeg -i output.mp4 -ss 00:00:01.000 -vframes 1 ISTEC.png && cd .. && mv tmp/ISTEC.png . && mv tmp/output.mp4 . && rm -rf tmp/


echo "
*start streaming* 

    python streamer.py

*start watchdog*

    python watcher.py

*start celery*

    celery worker -A tasks -l info

*catch streaming* 

    python stream_reader.py
"
