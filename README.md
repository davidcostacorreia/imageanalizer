# imageAnalizer

*start streaming*

    python streamer.py

*start watchdog*

    python watcher.py

*start celery*

    celery worker -A tasks -l info

*catch streaming*

    python stream_reader.py

 ---



**Instalation**

        sudo yum update -y 

        sudo rpm --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro 
        sudo rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm 

        sudo yum install epel-release wget screen git -y
        sudo yum install python-pip -y
        pip install --upgrade pip

        sudo reboot 

* #1 - **Install ElasticSearch**

        sudo yum install java-1.8.0-openjdk.x86_64 
        sudo rpm -ivh https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.2.3.rpm 

        sudo systemctl enable elasticsearch.service 

        sudo systemctl start elasticsearch.service 

        sudo systemctl status elasticsearch.service 

* #2 - **Install RabbitMQ**

        cd ~ 
        wget http://packages.erlang-solutions.com/erlang-solutions-1.0-1.noarch.rpm 
        sudo rpm -Uvh erlang-solutions-1.0-1.noarch.rpm 
        sudo yum install erlang -y 
 

        cd ~ 
        wget https://www.rabbitmq.com/releases/rabbitmq-server/v3.6.1/rabbitmq-server-3.6.1-1.noarch.rpm 
        sudo rpm --import https://www.rabbitmq.com/rabbitmq-signing-key-public.asc 
        sudo yum install rabbitmq-server-3.6.1-1.noarch.rpm -y 

 

        sudo firewall-cmd --zone=public --permanent --add-port=4369/tcp --add-port=25672/tcp --add-port=5671-5672/tcp --add-port=15672/tcp  --add-port=61613-61614/tcp --add-port=1883/tcp --add-port=8883/tcp 
        sudo firewall-cmd --reload 
 

        sudo systemctl start rabbitmq-server.service 
        sudo systemctl enable rabbitmq-server.service 
 

        sudo rabbitmqctl status 
 

        sudo rabbitmq-plugins enable rabbitmq_management 
        sudo chown -R rabbitmq:rabbitmq /var/lib/rabbitmq/ 
 

        sudo rabbitmqctl add_user admin admin 
        sudo rabbitmqctl set_user_tags admin administrator 
        sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"

* #3 - **Install Ffmpeg**

        sudo yum install ffmpeg ffmpeg-devel –y 


* #4 - **Get files**

        cd /opt/ && git clone https://davidcostacorreia@bitbucket.org/davidcostacorreia/imageanalizer.git
        cd imageanalizer
        pip install -r requirements.txt

* #5 - **Get a video and transform file**

Get Image from internet and transform it into a 5s mp4 video 

    wget http://inspiringfuture.pt/uploads/university/logo/265/ISTEC.png 

    ffmpeg -loop 1 -i ISTEC.png -c:v libx264 -b 800k -t 5 -pix_fmt yuv420p -vf scale=370:-1 imageVideo.mp4 

Get Sample video from internet and cut from 00:00:30 till 00:00:45 

    ffmpeg -i http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4 -ss 00:00:30 -t 15 BigBuckBunny.mp4 

Transform mp4 files into a Transport Stream file (TS) 

    ffmpeg -i imageVideo.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate1.ts 

    ffmpeg -i BigBuckBunny.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate2.ts 

Merge the 2 TS files into 1 

    ffmpeg -i "concat:intermediate1.ts|intermediate2.ts" -c copy -bsf:a aac_adtstoasc output.mp4 

Get frame to compare and override

    ffmpeg -i output.mp4 -ss 00:00:01.000 -vframes 1 ISTEC.png
