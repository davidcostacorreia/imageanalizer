
import time 
import datetime
import watchdog.events 
from tasks import processLog
from config import WatchFolder
from watchdog.observers import Observer 

class ScanFolder: 
    def __init__(self, path): 
        self.path = path 
        self.event_handler = watchdog.events.PatternMatchingEventHandler(patterns=WatchFolder['patterns'], 
                                    ignore_patterns=[], 
                                    ignore_directories=True) 

        self.event_handler.on_any_event = self.on_any_event 
        self.observer = Observer() 
        self.observer.schedule(self.event_handler, self.path, recursive=False) 
        self.observer.start() 


    def on_any_event(self, event): 
        if event.event_type != 'modified': 
            return 
        
        data = {
          'file': event.src_path,
          'sendToRabbitAt': datetime.datetime.now()
        }
        processLog.apply_async([{'_data': data}])
        print data

  
    def stop(self): 
        self.observer.stop() 
        self.observer.join() 


obj = ScanFolder('images/') 
try: 
    while True: 
        time.sleep(0.001) 
except: 
    obj.stop() 




