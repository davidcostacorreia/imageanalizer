
rabbitSettings = {
  'user': 'admin',
  'passwd': 'admin',
  'host': '127.0.0.1',
  'port': '5672',
  'vhost': ''
}

WatchFolder = {
    'patterns': ["*.jpg", "*.jpeg", "*.png"]
}

streaming = {
	'streamer': { #streamer.py
		'file': '/opt/imageanalizer/output.mp4',
		'udp': 'udp://127.0.0.1:5000'
	},
	'stream_reader': { #stream_reader.py
		'OutputArgs': '-vf fps=25', #collect 25 frames per second
		'imgPath':'/opt/imageanalizer/images/img%09d.jpg'
	}
}

compareImage = '/opt/imageanalizer/ISTEC.png'




